<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;


class NiagaController extends Controller
{
    public function index(Request $request){

    	$json = Storage::disk('local')->get('price_list.json');
    	$dt_json = json_decode($json,true);

    	//dd($json);
    	return view('welcome',compact('dt_json'));
    }
}
