<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">

	<title>Web Hosting Indonesia Unlimited & Terbaik - Niagahoster</title>
	<meta content="" name="description">
	<meta content="" name="keywords">

	<!-- Favicons -->
	<link href="https://www.niagahoster.co.id/assets/images/2019/favicon.png" rel="icon">
	<link href="https://www.niagahoster.co.id/assets/images/2019/favicon.png" rel="apple-touch-icon">

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

	<!-- Vendor CSS Files -->
	<link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{ asset('assets/vendor/icofont/icofont.min.css')}}" rel="stylesheet">
	<link href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
	<link href="{{ asset('assets/fontawesome/css/font-awesome.min.css')}}" rel="stylesheet">
	<link href="{{ asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
	<link href="{{ asset('assets/vendor/venobox/venobox.css')}}" rel="stylesheet">
	<link href="{{ asset('assets/vendor/aos/aos.css')}}" rel="stylesheet">

	<!-- Template Main CSS File -->
	<link href="{{ asset('assets/css/style.css')}}" rel="stylesheet">

	<!-- =======================================================
	* Template Name: BizLand - v1.2.1
	* Template URL: https://bootstrapmade.com/bizland-bootstrap-business-template/
	* Author: BootstrapMade.com
	* License: https://bootstrapmade.com/license/
	======================================================== -->
</head>

<body>

	<!-- ======= Top Bar ======= -->
	<div id="topbar" class="d-none d-lg-flex align-items-center fixed-top">
	<div class="container d-flex">
		<div class="contact-info mr-auto">
			<!-- <div class="toast" data-autohide="false"> -->
			<div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false">
				<div class="toast-header">
					<i class="fa fa-bookmark fa-2x text-primary" aria-hidden="true"></i>
					<small class="text-muted">Gratis Ebook 9 Cara Cerdas Menggunakan Domain</small>
					<button type="button" class="ml-2 mb-1 close" data-dismiss="toast">&times;</button>
				</div>
			</div>
		</div>
		<div class="social-links ">
			<i class="icofont-phone"></i> 0274-5305505
			<i class="icofont-speech-comments"></i> Live Chat
			<i class="icofont-user"></i> Member Area		
		</div>
	</div>
	</div>

	<!-- ======= Header ======= -->
	<header id="header" class="fixed-top">
	<div class="container d-flex align-items-center">

		<!-- <h1 class="logo mr-auto"><a href="index.html">BizLand<span>.</span></a></h1> -->
		<!-- Uncomment below if you prefer to use an image logo -->
		<a href="index.html" class="logo mr-auto"><img src="https://galayasa.com/wp-content/uploads/2020/12/niagahoster-logo.png" alt=""></a>

		<nav class="nav-menu d-none d-lg-block">
		<ul>
			<li class="active"><a href="index.html">Hosting</a></li>
			<li><a href="#domain">Domain</a></li>
			<li><a href="#server">Server</a></li>
			<li><a href="#website">Website</a></li>
			<li><a href="#afiliasi">Afiliasi</a></li>
			<li><a href="#promo">Promo</a></li>
			<li><a href="#pembayaran">Pembayaran</a></li>
			<li><a href="#review">Review</a></li>
			<li><a href="#kontak">Kontak</a></li>
			<li><a href="#blog">Blog</a></li>
		</ul>
		</nav><!-- .nav-menu -->

	</div>
	</header><!-- End Header -->

	<!-- ======= Hero Section ======= -->
	<section class="d-flex align-items-center">
		<!-- <div class="container" data-aos="zoom-out" data-aos-delay="100">
			<h1>Welcome to <span>BizLand</span></h1>
			<h2>We are team of talented designers making websites with Bootstrap</h2>
			<div class="d-flex">
				<a href="#about" class="btn-get-started scrollto">Get Started</a>
				<a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox btn-watch-video" data-vbtype="video" data-autoplay="true"> Watch Video <i class="icofont-play-alt-2"></i></a>
			</div>
		</div> -->
	</section><!-- End Hero -->

	<main id="main">

	<!-- ======= Home ======= -->
	<section class="d-flex align-items-center">
		<div class="container" data-aos="zoom-out" data-aos-delay="100">
			<div class="row">
				<div class="col-lg-6 pt-4 pt-lg-0 content d-flex flex-column justify-content-center" data-aos="fade-up" data-aos-delay="100">
					<h2>PHP Hosting</h2>
					<p>
						<h3 class="text-monospace">Cepat, handal, penuh dengan<br>modul PHP yang Anda butuhkan</h3>
					</p>
					<p>
						<ul class="list-unstyled">
							<li><span class="badge badge-success badge-pill"><i class="fa fa-check"></i></span> Solusi PHP untuk performa query yang lebih cepat</li>
							<li><span class="badge badge-success badge-pill"><i class="fa fa-check"></i></span> Konsumsi memori yang lebih rendah</i></li>
							<li><span class="badge badge-success badge-pill"><i class="fa fa-check"></i></span> Support PHP 5.3, PHP 5.4, PHP 5.5, PHP 7</i></li>
							<li><span class="badge badge-success badge-pill"><i class="fa fa-check"></i></span> Fitur enkripsi IonCube dan Zend Guard Loaders</i></li>
						</ul>
					</p>
				</div>
				<div class="col-lg-6" data-aos="zoom-out" data-aos-delay="100">
					<img src="assets/svg/illustration-banner-PHP-hosting-01.svg" class="img-fluid" alt="">
				</div>
			</div>
		</div>
	</section><!-- End Featured Services Section -->
	<hr>
	<section id="team" class="contact">
		<div class="container" data-aos="fade-up">
			<div class="row contact" data-aos="fade-up" data-aos-delay="100">
				<div class="col-lg-4 col-lg-2">
					<div class="info-box mb-4">
						<br><br><br>
						<object data="{{ asset('assets/svg/illustration-banner-PHP-zenguard01.svg') }}" type="image/svg+xml"></object>
						<br><br><br>
						<h3>PHP Zend Guard Loader</h3>
					</div>
				</div>

				<div class="col-lg-4">
					<div class="info-box  mb-4">
						<object data="{{ asset('assets/svg/icon-composer.svg') }}" type="image/svg+xml"></object>
						<br>
						<h3>PHP Composer</h3>
					</div>
				</div>

				<div class="col-lg-4 col-md-2">
				<div class="info-box  mb-4">
					<br><br><br>
					<object data="{{ asset('assets/svg/icon-php-hosting-ioncube.svg') }}" type="image/svg+xml"></object>
					<br><br><br>
					<h3>PHP ionCube Loader</h3>
				</div>
				</div>
			</div>
		</div>
	</section>
	<!-- ======= Pricing Section ======= -->
	<section id="pricing" class="pricing">
		<div class="container" data-aos="fade-up">
			<div class="section-title">
				<h3>Paket Hosting Singapura yang Tepat</h3>
				<h4>Diskon 40% + Domain dan SSL Gratis untuk Anda</h4>
			</div>
			<div class="row">
				@foreach($dt_json as $res_json)
				<div class="col-lg-3 col-md-6" data-aos="fade-up">
					<div class="box <?= $res_json['favorite'] == 'yes' ? 'featured':''; ?>">
						<?= $res_json['favorite'] == 'yes' ? '<span class="advanced">BEST SELLER</span>':''; ?>
						<h3 class="font-weight-bold"><?= $res_json['name']; ?></h3>
						<h6><s><?= $res_json['price'];?></s></h6>
						<h4><strong><sup>Rp</sup> 14<sup>.900/ bln</sup></strong></h4>
						<p><b><?= $res_json['total_user']; ?></b> Pengguna Terdaftar</p>
						<ul>
							@foreach($res_json['item'] as $item)
							<li><?= $item; ?></li>
							@endforeach
						</ul>
						<div class="btn-wrap">
							<a href="#" class="<?= $res_json['favorite'] == 'no'? 'btn-outline':'btn-buy-2'; ?>"><?= isset($res_json['discon']) ? 'Diskon '.$res_json['discon'].'%':'Pilih Sekarang'; ?></a>
						</div>
					</div>
				</div>
				@endforeach
				<!-- no foreach -->
			</div>
			<div class="clearfix">&nbsp;</div>
			<div class="row">
				<div class="col-lg-12 col-md-6">
					<h3 class="text-center">Powerful dengan Limit PHP yang Lebih Besar</h3>
					<div class="row">
						<div class="col-md-6">
							<div class="card">
								<ul class="list-group list-group-flush">
									<li class="list-group-item"><span class="badge badge-success badge-pill"><i class="fa fa-check"></i></span> max execution time 300s</li>
									<li class="list-group-item"><span class="badge badge-success badge-pill"><i class="fa fa-check"></i></span> max execution time 300s</li>
									<li class="list-group-item"><span class="badge badge-success badge-pill"><i class="fa fa-check"></i></span> php memory limit 1024 MB</li>
								</ul>
							</div>
						</div>
						<div class="col-md-6">
							<div class="card">
								<ul class="list-group list-group-flush">
									<li class="list-group-item"><span class="badge badge-success badge-pill"><i class="fa fa-check"></i></span> post max size 128 MB</li>
									<li class="list-group-item"><span class="badge badge-success badge-pill"><i class="fa fa-check"></i></span> upload max filesize 128 MB</li>
									<li class="list-group-item"><span class="badge badge-success badge-pill"><i class="fa fa-check"></i></span> max input vars 2500</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section><!-- End Pricing Section -->

	<!-- ======= Pricing Section ======= -->
	<section id="paket" class="services">
		<div class="container" data-aos="fade-up">
			<div class="section-title">
				<h3>Semua Paket Hosting Sudah Termasuk</h3>
			</div>

			<div class="row">
				<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
					<div class="icon-box">
						<div class="icon"><img src="assets/svg/php_semua_versi.svg" class="img-fluid" /></div>
						<h4>PHP Semua Versi</h4>
						<p>Pilih mulai dari versi PHP 5.3 s/d PHP 7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br>Ubah sesuka Anda!</p>
					</div>
				</div>

				<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="200">
					<div class="icon-box">
						<div class="icon"><img src="assets/svg/icon-PHP-Hosting_My SQL.svg" class="img-fluid" /></div>
						<h4>MySQL Versi 5.6</h4>
						<p>Nikmati MySQL versi terbaru, tercepat dan <br>kaya akan fitur</p>
					</div>
				</div>

				<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
					<div class="icon-box">
						<div class="icon"><img src="assets/svg/icon-PHP-Hosting_CPanel.svg" class="img-fluid" /></div>
						<h4>Panel Hosting cPanel</h4>
						<p>Kelola website dengan panel canggin yang <br>familiar di hati anda</p>
					</div>
				</div>

				<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
					<div class="icon-box">
						<div class="icon"><img src="assets/svg/icon-PHP-Hosting_garansi uptime.svg" class="img-fluid" /></div>
						<h4>Garansi Uptime 99.9%</h4>
						<p>Data center yang mendukung kelangsungan <br>website Anda 24/7</p>
					</div>
				</div>

				<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
					<div class="icon-box">
						<div class="icon"><img src="assets/svg/icon-PHP-Hosting_InnoDB.svg" class="img-fluid" /></div>
						<h4>Database InnoDB Unlimited</h4>
						<p>Jumlah dan ukuran database yang tumbuh <br>sesuai dengan kebutuhan Anda.</p>
					</div>
				</div>

				<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
					<div class="icon-box">
						<div class="icon"><img src="assets/svg/icon-PHP-Hosting_My SQL remote.svg" class="img-fluid" /></div>
						<h4>Wilcard Remote MySQL</h4>
						<p>Mendukung s/d 25 max_user_connections <br> dan 100 max_connection</p>
					</div>
				</div>
				
			</div>
		</div>
	</section>

	<!-- ======= Frequently Asked Questions Section ======= -->
	<section id="faq" class="faq section-bg">
		<div class="container" data-aos="fade-up">
			<div class="section-title">
				<h3>Mendukung Penuh Framework Laravel</h3>
			</div>

			<div class="row">
				<div class="col-lg-6 pt-4 pt-lg-0 content d-flex flex-column" data-aos="fade-up" data-aos-delay="100">
					<p class="text-monospace">
						Tak perlu menggunakan dedicated server ataupun VPS yang mahal. Layanan PHP hosting murah kami mendukung penuh framework favorit Anda.
					</p>
					<ul class="list-unstyled">
						<li><span class="badge badge-success badge-pill"><i class="fa fa-check"></i></span> Install Laravel <b>1 Klik</b> dengan Softacolus Installer.</li>
						<li><span class="badge badge-success badge-pill"><i class="fa fa-check"></i></span> Mendukung ekstensi <b>PHP MCrypt, phar, mbstring, json, dan fileinfo.</b></li>
						<li><span class="badge badge-success badge-pill"><i class="fa fa-check"></i></span> Tersedia Composer dan SSH untuk menginstall packages pilihan Anda.</li>
					</ul>
					<span><small>Nb. Composer dan SSH hanya tersedia pada paket Personal dan Bisnis</small></span>
					
					<div class="text-left"><button type="submit" class="btn btn-blue">Pilih Hosting Anda</button></div>
				</div>
				<div class="col-lg-6" data-aos="zoom-out" data-aos-delay="100">
					<img src="assets/svg/illustration-banner-PHP-hosting-01.svg" class="img-fluid" alt="">
				</div>
			</div>
		</div>
	</section><!-- End Frequently Asked Questions Section -->

	<!-- ======= Contact Section ======= -->
	<section id="faq" class="contact">
		<div class="container" data-aos="fade-up">
			<div class="section-title">
				<h3>Modul Lengkap untuk Menjalankan Aplikasi PHP Anda.</h3>
			</div>

			<div class="row" data-aos="fade-up" data-aos-delay="100">
				<div class="col-lg-3">
					<div class="info-box mb-4">
						<ul class="list-unstyled">
							<li>IcePHP</li>
							<li>apc</li>
							<li>apcu</li>
							<li>apm</li>
							<li>ares</li>
							<li>bcmath</li>
							<li>bcompiler</li>
							<li>big_int</li>
							<li>bitset</li>
							<li>bloomy</li>
							<li>bz2_filter</li>
							<li>clamav</li>
							<li>coin_acceptor</li>
							<li>crack</li>
							<li>dba</li>
						</ul>
					</div>
				</div>

				<div class="col-lg-3">
					<div class="info-box mb-4">
						<ul class="list-unstyled">
							<li>IcePHP</li>
							<li>apc</li>
							<li>apcu</li>
							<li>apm</li>
							<li>ares</li>
							<li>bcmath</li>
							<li>bcompiler</li>
							<li>big_int</li>
							<li>bitset</li>
							<li>bloomy</li>
							<li>bz2_filter</li>
							<li>clamav</li>
							<li>coin_acceptor</li>
							<li>crack</li>
							<li>dba</li>
						</ul>
					</div>
				</div>

				<div class="col-lg-3">
					<div class="info-box mb-4">
						<ul class="list-unstyled">
							<li>IcePHP</li>
							<li>apc</li>
							<li>apcu</li>
							<li>apm</li>
							<li>ares</li>
							<li>bcmath</li>
							<li>bcompiler</li>
							<li>big_int</li>
							<li>bitset</li>
							<li>bloomy</li>
							<li>bz2_filter</li>
							<li>clamav</li>
							<li>coin_acceptor</li>
							<li>crack</li>
							<li>dba</li>
						</ul>
					</div>
				</div>

				<div class="col-lg-3">
					<div class="info-box mb-4">
						<ul class="list-unstyled">
							<li>IcePHP</li>
							<li>apc</li>
							<li>apcu</li>
							<li>apm</li>
							<li>ares</li>
							<li>bcmath</li>
							<li>bcompiler</li>
							<li>big_int</li>
							<li>bitset</li>
							<li>bloomy</li>
							<li>bz2_filter</li>
							<li>clamav</li>
							<li>coin_acceptor</li>
							<li>crack</li>
							<li>dba</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="text-center pricing btn-wrap">
						<a href="#" class="btn-outline">Selengkapnya</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Contact Section -->
	<section id="paket" class="services">
		<div class="container" data-aos="fade-up">
			<div class="row">
				<div class="col-lg-6 pt-4 pt-lg-0 content d-flex flex-column justify-content-center" data-aos="fade-up" data-aos-delay="200">
					<p class="text-monospace">
						<h4>Linux Hosting yang Stabli dengan Teknologi LVE</h4>
					</p>
					<p class="text-justify">
						SuperMicro <b>Intel Xeon 24-Cores</b> server dengan RAM <b> 128 GB</b> dan teknologi <b>LVE CloudLinux</b> untuk stabilitas server Anda. Dilengkapi dengan <b>SSD</b> untuk kecepatan <b>MySQL</b> dan caching, Apache load balancer berbasis LiteSpeed Technologies, <b>CageFS</b> security, <b>Raid-10</b> protection dan auto backup untuk keamanan website PHP Anda.
					</p>					
					<div class="text-left"><button type="submit" class="btn btn-blue">Pilih Hosting Anda</button></div>
				</div>
				<div class="col-lg-6" data-aos="zoom-out" data-aos-delay="100">
					<img src="assets/img/Image- support.png" class="img-fluid" alt="">
				</div>
			</div>
		</div>
	</section>

	<section id="info" class="info section-info">
		<div class="container">
			<div class="row">
				<div class="col-lg-7 pt-4 pt-lg-0 content d-flex flex-column justify-content-center">
					Bagikan jika Anda menyukai halaman ini.
				</div>
				<div class="col-lg-5">
					<span class="fa-stack fa-lg text-primary">
					<i class="fa fa-square fa-stack-2x"></i>
					<i class="fa fa-facebook fa-stack-1x fa-inverse"></i></span>
					<span class="badge badge-light">80k</span>

					<span class="fa-stack fa-lg text-success">
					<i class="fa fa-square fa-stack-2x"></i>
					<i class="fa fa-twitter fa-stack-1x fa-inverse"></i></span>
					<span class="badge badge-light">450</span>

					<span class="fa-stack fa-lg text-danger">
					<i class="fa fa-square fa-stack-2x"></i>
					<i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
					</span>
					<span class="badge badge-light">1900</span>
				</div>
			</div>
		</div>
	</section>
	</main><!-- End #main -->

	<!-- ======= Footer ======= -->
	<footer id="footer">

		<div class="footer-newsletter">
			<div class="container">
				<div class="row">
					<div class="col-lg-9">
						<h1>Perlu BANTUAN? Hubungi Kami : 0274-5305505</h1>
					</div>
					<div class="col-lg-3 line">
						<div class="text-center">
							<a href="#" class="btn-outline"><i class="icofont-speech-comments"></i>Live Chat</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-6 footer-contact">
						<h4>HUBUNGI KAMI</h4>
						<p>
							0274-5305505<br>
							Senin - Minggu<br>
							24 Jam Nonstop<br><br>

							Jl. Selokan Mataram Monjali<br>
							Karangjati MT 1/304<br>
							Sinduadi, Mlati, Sleman<br>
							Yogyakarta 55284
						</p>
					</div>

					<div class="col-lg-3 col-md-6 footer-contact">
						<h4>LAYANAN</h4>
						<a href="#">Domain</a><br>
						<a href="#">Shared Hosting</a><br>
						<a href="#">Cloud VPS Hosting</a><br>
						<a href="#">Managed VPS Hosting</a><br>
						<a href="#">Web Builder</a><br>
						<a href="#">Keamanan SSL / HTTPS</a><br>
						<a href="#">Jasa Pembuatan Website</a><br>
						<a href="#">Program Afiliasi</a><br>
					</div>

					<div class="col-lg-3 col-md-6 footer-contact">
						<h4>SERVICE HOSTING</h4>
						<a href="#">Hosting Murah</a><br>
						<a href="#">Hosting Indonesia</a><br>
						<a href="#">Hosting Singapura SG</a><br>
						<a href="#">Hosting PHP</a><br>
						<a href="#">Hosting Wordpres</a><br>
						<a href="#">Hosting Laravel</a><br>
					</div>

					<div class="col-lg-3 col-md-6 footer-contact">
						<h4>TUTORIAL</h4>
						<a href="#">Knowledgebase</a><br>
						<a href="#">Blog</a><br>
						<a href="#">Cara Pembayaran</a><br>
					</div>

					<div class="col-lg-3 col-md-6 footer-contact">
						<h4>TENTANG KAMI</h4>
						<a href="#">Tim Niagahoster</a><br>
						<a href="#">Karir</a><br>
						<a href="#">Events</a><br>
						<a href="#">Penawaran & Promo Spesial</a><br>
						<a href="#">Kontak Kami</a><br>
					</div>

					<div class="col-lg-3 col-md-6 footer-contact">
						<h4>KENAPA PILH NIAGAHOSTER?</h4>
						<a href="#">Tim Niagahoster</a><br>
						<a href="#">Karir</a><br>
						<a href="#">Events</a><br>
						<a href="#">Penawaran & Promo Spesial</a><br>
						<a href="#">Kontak Kami</a><br>
					</div>

					<div class="col-lg-3 col-md-6 footer-contact">
						<h4>NEWSLETTER</h4>
						<form action="" method="post">
							<input type="email" name="email" placeholder="Email"><input type="submit" value="Berlangganan">
						</form>
						<p>Dapatkan Promo dan Konten Menarik dari penyedia hosting terbaik Anda</p>
					</div>
					<div class="col-lg-3 col-md-6 footer-contact">
						<div class="social-links mt-3">
							<a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
							<a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
							<a href="#" class="google"><i class="bx bxl-google-plus"></i></a>
						</div>
					</div>
					<div class="col-lg-12 col-md-6 footer-contact">
						<h4>PEMBAYARAN</h4>
						<img src="http://1.bp.blogspot.com/-7NI-21ViLXE/VIqF5oVRf3I/AAAAAAAAA3o/67vDcoumATw/s1600/Bank%2BBCA.png" width="	80">
						<img src="https://img2.pngdownload.id/20180816/pqs/kisspng-bank-mandiri-logo-credit-card-portable-network-gra-kintakun-single-hello-kitty-bed-cover-murah-gros-5b75a7f2e0abc4.4324712015344373629203.jpg" width="80">
						<img src="https://upload.wikimedia.org/wikipedia/id/thumb/5/55/BNI_logo.svg/1200px-BNI_logo.svg.png" width="80">
						<br><br>
						Aktivasi instant dengan e-Payment. Hosting dan Domain langsung Aktif!
						<hr><br>
						<div class="copyright">
							Copyright &copy; 2016 Niagahoster| Hosting Powered by PHP7, CloudLinux, CloudFlare, BitNinja and DC Biznet Technovillage Jakarta<br>Cloud VPS Murah Powered by Webuzo Softaculous, Intel SSD and cloud computing technology.
						</div>
						<div class="credits">
						Syarat dan Ketentuan | Kebijakan Privasi
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!-- End Footer -->

	<div id="preloader"></div>
	<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

	<!-- Vendor JS Files -->
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
	<script src="assets/vendor/php-email-form/validate.js"></script>
	<script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
	<script src="assets/vendor/counterup/counterup.min.js"></script>
	<script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
	<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
	<script src="assets/vendor/venobox/venobox.min.js"></script>
	<script src="assets/vendor/aos/aos.js"></script>

	<!-- Template Main JS File -->
	<script src="assets/js/main.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.toast').toast('show');
		});
	</script>
</body>

</html>